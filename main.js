const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const crypto = require('crypto');
const credentials = require('./credentials.json').credentials;

app.use(express.static('public'));
app.use(bodyParser.json());

app.post('/login', (req, res) => {
    const {username, password} = req.body;
    const success = credentials.some(
        credential => credential.username === username && credential.password === hash(password)
    );
    const loginResult = {
        success: success,
        errorMessage: success ? '' : 'Invalid Credentials'
    };
    const statusCode = success ? 200 : 401;
    setTimeout(() => res.status(statusCode).send(loginResult), randomDelay());
});

app.post('/logout', (req, res) => {
    setTimeout(() => res.status(200).send(), randomDelay());
});

app.listen(3000);

function randomDelay() {
    return Math.random()*2000 + 1000;
}

function hash(password) {
    return crypto.createHash('sha256').update(password, 'utf8').digest('hex');
}
