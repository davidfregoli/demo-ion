const demoToolbarComponent = {
	templateUrl: 'toolbar.html',
	controller: ['loginService', function (loginService) {
		this.status = loginService.getStatus();
		this.logout = loginService.logout;
		this.toggleModal = loginService.toggleModal;
	}]
};

const loginModalComponent = {
	templateUrl: 'login.html',
	controller: ['loginService', function (loginService) {
		this.status = loginService.getStatus();
		this.toggleModal = loginService.toggleModal;
		this.resetErrorMsg = loginService.resetErrorMsg;
		this.login = () => loginService.login(this.username, this.password);
	}]
};

const demoAppController = function (loginService) {
	this.status = loginService.getStatus();
};

angular
	.module('DemoApp', [])
	.controller('DemoAppController', ['loginService', demoAppController])
	.factory('loginService', ['$q', '$http', loginService])
	.component('demoToolbar', demoToolbarComponent)
	.component('loginModal', loginModalComponent);

function loginService($q, $http) {
	const _status = {
		is_logged_in: false,
		is_loading: false,
		show_modal: false,
		mock_calls: false,
		errorMessage: ''
	};

	return {
		resetErrorMsg() {
			_status.errorMessage = '';
		},
		logout() {
			let call;

			// disable UI inputs
			_status.is_loading = true;

			if (_status.mock_calls) {
				let deferred = $q.defer();
				call = deferred.promise;
				deferred.resolve();
			} else {
				call = $http.post('/logout');
			}

			call.then(response => {
				_status.is_loading = false;
				_status.is_logged_in = false;
			});
		},
		login(username, password) {
			let call;

			// disable UI inputs
			_status.is_loading = true;

			if (_status.mock_calls) {
				let deferred = $q.defer();
				call = deferred.promise;
				if (username === 'admin' && password === 'password') {
					deferred.resolve({
						data: {
							success: true,
							errorMessage: ''
						}
					});
				} else {
					deferred.reject({
						data: {
							success: false,
							errorMessage: 'Invalid Credentials'
						}
					});
				}
			} else {
				call = $http.post('/login', {username, password});
			}

			call.then(response => {
				// successful login
				_status.is_loading = false;
				_status.show_modal = false;
				_status.is_logged_in = true;
				_status.errorMessage = '';
			}, response => {
				// failed login
				_status.is_loading = false;
				_status.errorMessage = response.data.errorMessage;
			});
		},
		getStatus() {
			return _status;
		},
		toggleModal() {
			_status.show_modal = !_status.show_modal;
		}
	};
}
